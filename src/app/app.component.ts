import { Component } from '@angular/core';
import { NgxStatusNetworkService } from 'projects/ngx-status-network/src/public-api';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'test-status-network';

    constructor(private _statusNetwork: NgxStatusNetworkService) {
        this._statusNetwork.networkStatusChangeDetection({
            stableNetwork: '',
            lowNetwork: '',
            notNetwork: ''
        });
    }
}
