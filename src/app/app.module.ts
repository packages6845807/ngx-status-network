import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NgxStatusNetworkModule } from 'projects/ngx-status-network/src/public-api';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        NgxStatusNetworkModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
