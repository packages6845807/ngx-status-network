# Status network (Estado de la red)

## Instalacion
```
npm i ngx-status-network
```

## Importar modulo
```ts
@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        NgxStatusNetworkModule 
    ],
    providers: [],
    bootstrap: [AppComponent]
})
```

## Inicializarlo

```ts
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'test-status-network';

    constructor(private _statusNetwork: NgxStatusNetworkService) {
        this._statusNetwork.networkStatusChangeDetection();
    }
}
```

## Tipos de alertas
Estas alertas se muestran cuando hay un cambio en la red.

### Red estable
![autor](./src/assets/alert-success.PNG)

### Red baja
![autor](./src/assets/alert-warn.PNG)

### Red no establecida
![autor](./src/assets/alert-danger.PNG)

## Ajustes de personalizados
Inicialmente la libreria carga unos mensajes por defecto, pero
se pueden personalizar de la siguiente manera a la hora de instanciar en 
el constructor de la clase.
```ts
    constructor(private _statusNetwork: NgxStatusNetworkService) {
        this._statusNetwork.networkStatusChangeDetection({
            stableNetwork: '', /* Mensaje de conexion estable */
            lowNetwork: '', /* Mensaje de conexion lenta */
            notNetwork: '' /* Mensaje de conexion no establecida */
        });
    }
```

![autor](./src/assets/footer.PNG)
