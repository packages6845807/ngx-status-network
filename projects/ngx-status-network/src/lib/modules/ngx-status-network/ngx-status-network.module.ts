import { NgModule } from '@angular/core';
import { NgxStatusNetworkComponent } from './components/ngx-status-network.component';
import { CommonModule } from '@angular/common';



@NgModule({
    declarations: [
        NgxStatusNetworkComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        NgxStatusNetworkComponent
    ]
})
export class NgxStatusNetworkModule { }
