import { Component } from '@angular/core';
import { NavigationStatus } from '../interfaces/navigation-status.interface';
import { NgxStatusNetworkService } from '../services/ngx-status-network.service';

@Component({
    selector: 'ngx-status-network',
    templateUrl: './ngx-status-network.component.html',
    styleUrls: ['./ngx-status-network.component.scss']
})
export class NgxStatusNetworkComponent {

    public hasViewStatus: NavigationStatus = { high: false, medium: false, low: false };

    private _previousViewStatus: NavigationStatus = this.hasViewStatus;


    constructor(
        private _statusNetwork: NgxStatusNetworkService
    ) {
        this._initObs();
    }

    /**
     * @name _initObs
     * @description
     * Se inicializa el observable donde estara a la
     * espera de cambios en la conexion a internet
     */
    private _initObs() {
        this._statusNetwork.navigationStatus$.subscribe({
            next: (status: NavigationStatus | null) => {
                if (status) {
                    if (JSON.stringify(status) !== JSON.stringify(this._previousViewStatus)) {
                        this.hasViewStatus = status;
                        this._previousViewStatus = this.hasViewStatus;
                    }
                }
            }
        });
    }

    /**
     * @name hideAlert
     * @description
     * Oculta la barra del mensaje
     */
    public hideAlert() {
        this.hasViewStatus = { high: false, medium: false, low: false };
    }

}
