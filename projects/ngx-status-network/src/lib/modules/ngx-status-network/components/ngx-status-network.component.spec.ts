import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxStatusNetworkComponent } from './ngx-status-network.component';

describe('NgxStatusNetworkComponent', () => {
  let component: NgxStatusNetworkComponent;
  let fixture: ComponentFixture<NgxStatusNetworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxStatusNetworkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NgxStatusNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
