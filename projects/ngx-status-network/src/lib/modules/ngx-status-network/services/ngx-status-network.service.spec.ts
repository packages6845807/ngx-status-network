import { TestBed } from '@angular/core/testing';

import { NgxStatusNetworkService } from './ngx-status-network.service';

describe('NgxStatusNetworkService', () => {
  let service: NgxStatusNetworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgxStatusNetworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
