import { Injectable } from '@angular/core';
import { CustomNavigationParmas, NavigationStatus } from '../interfaces/navigation-status.interface';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class NgxStatusNetworkService {



    private _navigationStatus$: BehaviorSubject<NavigationStatus> = new BehaviorSubject<NavigationStatus>({ high: false, medium: false, low: false });

    constructor() { }

    /**
     * @method get&set_navigationStatus
     */
    public get navigationStatus$(): Observable<NavigationStatus> {
        return this._navigationStatus$.asObservable();
    }
    public set navigationStatusReset$(value: NavigationStatus) {
        this._navigationStatus$.next(value);
    }

    /**
     * @method networkStatusChangeDetection
     * @description
     * Cada 5s se verifica el estatus de coneccion de la red,
     * con eso validamos que tipo de alerta mostrará por medio del estatus de red.
     *  - verde: 4g
     *  - naranja: 3g, 2g >
     *  - roja: cuando no hay conexion de red
     */
    networkStatusChangeDetection(params?: CustomNavigationParmas) {
        setInterval(() => {
            let networkingSpeed: any = window.navigator.connection;
            let hasConnected: boolean = window.navigator.onLine;
            const status: NavigationStatus = {
                high: networkingSpeed.effectiveType == "4g" && networkingSpeed.downlink > 0 && hasConnected ? true : false,
                medium: (networkingSpeed.effectiveType == "3g" || networkingSpeed.effectiveType == "2g") && networkingSpeed.downlink > 0 && hasConnected ? true : false,
                low: networkingSpeed.effectiveType == "4g" && networkingSpeed.downlink === 0 && !hasConnected ? true : false,
            }

            this.navigationStatusReset$ = {
                ...status,
                customNavigationParmas: params
            }

        }, 5000)
    }
}
