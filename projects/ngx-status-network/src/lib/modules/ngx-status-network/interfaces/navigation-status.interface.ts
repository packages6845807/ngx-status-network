/**
 * @name NavigationStatus
 * @description
 * low: red baja conectividad
 * medium: red media conectividad
 * high: red alta conectividad
 */
export interface NavigationStatus {
    high: boolean;
    medium: boolean;
    low: boolean;
    customNavigationParmas?: CustomNavigationParmas
}

export interface CustomNavigationParmas {
    stableNetwork?: string;
    // stableNetworkBg?: string;
    // stableNetworkColor?: string;
    lowNetwork?: string;
    // lowNetworkBg?: string;
    // lowNetworkColor?: string;
    notNetwork?: string;
    // notNetworkBg?: string;
    // notNetworkColor?: string;
}


